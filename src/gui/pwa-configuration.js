/** @type {import('vite-plugin-pwa').VitePWAOptions} */
const pwaConfiguration = {
  // Precache
  //includeAssets: ['data/kklc/*.json'],
  workbox: {
    runtimeCaching: [
      {
        //https://trailsinthedatabase.com/api/script/detail/:gameid/:fname
        urlPattern: /^https:\/\/trailsinthedatabase.com\/api\/script\/detail\/{0-9}{1,2}\/.*/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'script-files',
          expiration: {
            maxEntries: 3333,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/kklc\/[0-9]{4}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'kklc-entries',
          expiration: {
            maxEntries: 2302,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/frequency\/[0-9]{4}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'frequency-entries',
          expiration: {
            maxEntries: 4583,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/grade\/[0-9]{2}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'grade-entries',
          expiration: {
            maxEntries: 12,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/heisig5\/[0-9]{4}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'heisig5-entries',
          expiration: {
            maxEntries: 3032,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/heisig6\/[0-9]{4}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'kklc-entries',
          expiration: {
            maxEntries: 3003,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/jlpt\/[0-9]{1}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'jlpt-entries',
          expiration: {
            maxEntries: 7,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/kanken\/[0-9]{2}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'kanken-entries',
          expiration: {
            maxEntries: 12,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
      {
        urlPattern: /^https:\/\/.*\/data\/wk\/[0-9]{2}\.json$/i,
        handler: 'CacheFirst',
        options: {
          cacheName: 'wk-entries',
          expiration: {
            maxEntries: 62,
            maxAgeSeconds: 60 * 60 * 24 * 365 // <== 365 days
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      },
    ]
  },
  manifest: {
    name: 'Trails of Reading',
    short_name: 'TrailsRead',
    description: 'Reading drills straight outta Zemuria.',
    start_url: './',
    scope: './',
    theme_color: '#3e0909',
    background_color: '#3e0909',
    display: 'standalone',
    orientation: 'portrait',
    icons: [
      {
        src: './android-chrome-192x192.png',
        sizes: '192x192',
        type: 'image/png'
      },
      {
        src: './android-chrome-512x512.png',
        type: 'image/png',
        sizes: '512x512'
      },
      {
        src: './android-chrome-512x512.png',
        type: 'image/png',
        sizes: '512x512',
        purpose: 'maskable'
      }
    ]
  },
};

export { pwaConfiguration };